<div align="center">

# Collection of useful aliases for Linux.

</div>

### Disclaimer

Aliases contain absolute paths or URLs. Be aware to change it according to your system.

---
### Navigation

- [Setup](#setup)
- [Aliases](#aliases)

---
### Setup

- open **.bashrc** in text editor eg. `subl ~/.bashrc` and paste aliases at the end of the file
- save **.bashrc** and load `source ~/.bashrc`

---
### Aliases

```
# CUSTOM

# open current path in file explorer
alias filex='xdg-open .'

# get IP
alias myip='echo $(curl --silent "ifconfig.me")'

# get MAC address
alias mymac='INTERFACE=$(get_network_interface); MAC=$(cat /sys/class/net/${INTERFACE}/address); echo ${INTERFACE} - ${MAC}'

get_network_interface() {
  local -r HOST=google.com
  local -r HOST_IP=$(getent ahosts "${HOST}" | awk '{print $1; exit}')
  
  ip route get "${HOST_IP}" | grep -Po '(?<=(dev )).*(?= src| proto)'
}

# get local IP
alias mylocalip="ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'"

# get all hosts on local network
alias listlocalhosts='sudo arp-scan --interface=wlp1s0 --localnet'

# use vpn connection
alias opn="python3 '/home/${USER}/Documents/gitlab-projects/openpyn-simple-python/app.py'"

# switch back to default connection
alias opnx="python3 '/home/${USER}/Documents/gitlab-projects/openpyn-simple-python/app.py' -d && exit"

# get systemctl and journalctl errors
alias generrreport="sudo systemctl --failed > ~/Desktop/error-report.txt && printf '\n\n' >> ~/Desktop/error-report.txt && sudo journalctl -p 3 -xb >> ~/Desktop/error-report.txt"

# disable "Cinnamon Settings Daemon - xrandr" in startup applications, since it disables laptop screen when connected to external monitor
# fix when laptop screen is already black when connected to external monitor
alias fixxrandr="xrandr --output HDMI-1 --auto --primary --output eDP-1 --auto --left-of HDMI-1"

# END OF CUSTOM
```
